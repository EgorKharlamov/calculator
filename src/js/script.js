let calculator = document.querySelector('.calculator');
let allRadioBtn = calculator.querySelectorAll('.radio-btn');
let current = calculator.querySelectorAll('.current');
let rate = calculator.querySelector('.rate');
let perMonth = calculator.querySelector('.per-month');
let sumMonth = calculator.querySelector('.sum-month');
let P = current[2].getElementsByTagName('input')[0];
let priceForP = current[1].getElementsByTagName('input')[0];
let maintenancePrice = calculator.querySelector('.maintenance-price');
let spendForOne = calculator.querySelector('.spendForOne');
let spendEachMonth = calculator.querySelector('.spendEachMonth');
let spendWithGpu = calculator.querySelector('.spendWithGpu');
let lux = calculator.querySelector('.luxury');
let paybackTime = calculator.querySelector('.paybackTime');
let dropdownSelect = calculator.querySelector('.dropdown-select');

let maintenance = calculator.querySelector('.maintenance');
let candlePrice = maintenance.getElementsByTagName('p')[1].getElementsByTagName('span')[0];
let filters = maintenance.getElementsByTagName('p')[3].getElementsByTagName('span')[0];
let oilCapacity = maintenance.querySelector('.ul').getElementsByTagName('p')[1].getElementsByTagName('span')[0];
let changeOil = maintenance.querySelector('.ul').getElementsByTagName('p')[3].getElementsByTagName('span')[0];
let addOil = maintenance.querySelector('.ul').getElementsByTagName('p')[5].getElementsByTagName('span')[0];
let oilPrice = maintenance.querySelector('.ul').getElementsByTagName('p')[7].getElementsByTagName('span')[0];
let oilFullPrice = maintenance.querySelector('.ul').getElementsByTagName('p')[9].getElementsByTagName('span')[0];

// ---------------CREATEELEMENT---------------
let createElem = (tagName, className, text) => {
  let element = document.createElement(tagName);
  element.classList.add(className);
  if (text) {
    element.textContent = text;
  }
  return element;
}
// -------------------------------------------

// ----------RADIO----------
allRadioBtn.forEach(elementRadio => {
  elementRadio.addEventListener('click', function () {
    allRadioBtn.forEach(anotherElement => {
      if (anotherElement != elementRadio) {
        input = anotherElement.querySelector('.input-radio');
        input.checked = false;
      }
    })
    input = elementRadio.querySelector('.input-radio');
    input.checked = true;
    if (dropdownSelect.innerHTML) {
      dropdownSelect.innerHTML = '';
    }
    if (input.checked) {
      if (elementRadio.textContent == 100) {
        stations100.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
      else if (elementRadio.textContent == 150) {
        stations150.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
      else if (elementRadio.textContent == 200) {
        stations200.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
      else if (elementRadio.textContent == 250) {
        stations250.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
      else if (elementRadio.textContent == 300) {
        stations300.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
      else if (elementRadio.textContent == 450) {
        stations450.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
      else if (elementRadio.textContent == 500) {
        stations500.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
      else if (elementRadio.textContent == 650) {
        stations650.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
      else if (elementRadio.textContent == 700) {
        stations700.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
      else if (elementRadio.textContent == 750) {
        stations750.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
      else if (elementRadio.textContent == 1000) {
        stations1000.forEach(station => {
          let dropdownOption = createElem('option', 'dropdown-option', station.name);
          dropdownSelect.appendChild(dropdownOption);
        })
      }
    }


    allStations.forEach(stationsArr => {
      stationsArr.forEach(item => {
        if (item.name == dropdownSelect.value) {
          candlePrice.textContent = item.maintStation1 + ' руб.';
          filters.textContent = item.maintStation2 + ' руб.';
          oilCapacity.textContent = item.maintStation3 + ' л';
          changeOil.textContent = item.maintStation4;
          addOil.textContent = item.maintStation5 + ' л/мес';
          oilPrice.textContent = item.maintStation6 + ' руб.';
          oilFullPrice.textContent = item.maintStation7 + ' руб.';
          calcMaintenance(+parseFloat(candlePrice.textContent), +parseFloat(filters.textContent), +parseFloat(oilCapacity.textContent), +parseFloat(changeOil.textContent), +parseFloat(oilPrice.textContent), +parseFloat(addOil.textContent));

          P.value = calcP(elementRadio.textContent);
          rate.textContent = calcRate(elementRadio.textContent);
          perMonth.textContent = calcPerMonth(rate.textContent);
          sumMonth.textContent = calcSumMonth(current[0].getElementsByTagName('input')[0].value, parseInt(perMonth.textContent)) + ' руб.';
          spendForOne.textContent = spendingForOne(parseInt(sumMonth.textContent), parseInt(maintenancePrice.textContent), parseInt(P.value)) + ' руб.';
          spendEachMonth.textContent = currentSpending(parseInt(P.value), parseInt(priceForP.value)) + ' руб.';
          spendWithGpu.textContent = spendWithGPU(parseInt(sumMonth.textContent), parseInt(maintenancePrice.textContent)) + ' руб.';
          lux.textContent = economy(parseInt(spendEachMonth.textContent), parseInt(spendWithGpu.textContent)) + ' руб.';
          paybackTime.textContent = payback(item.price, lux.textContent) + ' месяцев';

          // -------------INPUT-------------
          current.forEach(element => {
            let currentInput = element.getElementsByTagName('input')[0];
            currentInput.addEventListener('change', () => {
              // P.value = calcP(elementRadio.textContent);
              // rate.textContent = calcRate(elementRadio.textContent);
              let getRadio = 0;
              allRadioBtn.forEach(element => {
                let input = element.getElementsByTagName('input')[0];
                if (input.checked) {
                  getRadio = element.textContent;
                }
              })
              P.value = calcP(getRadio);
              perMonth.textContent = calcPerMonth(parseInt(rate.textContent));
              sumMonth.textContent = calcSumMonth(parseInt(current[0].getElementsByTagName('input')[0].value), parseInt(perMonth.textContent)) + ' руб.';
              spendForOne.textContent = spendingForOne(parseInt(sumMonth.textContent), parseInt(maintenancePrice.textContent), +P.value) + ' руб.';
              spendEachMonth.textContent = currentSpending(+P.value, +priceForP.value) + ' руб.';
              spendWithGpu.textContent = spendWithGPU(parseInt(sumMonth.textContent), parseInt(maintenancePrice.textContent)) + ' руб.';
              lux.textContent = economy(parseInt(spendEachMonth.textContent), parseInt(spendWithGpu.textContent)) + ' руб.';
              // !!!!!!!!!!!!!!!!!!!

              paybackTime.textContent = payback(3100000, parseInt(lux.textContent)) + ' месяцев';
            });
          })
          // -------------------------------
        }
      })
    })
    dropdownSelect.addEventListener('change', () => {
      allStations.forEach(stationsArr => {
        stationsArr.forEach(item => {
          if (item.name == dropdownSelect.value) {
            candlePrice.textContent = item.maintStation1 + ' руб.';
            filters.textContent = item.maintStation2 + ' руб.';
            oilCapacity.textContent = item.maintStation3 + ' л';
            changeOil.textContent = item.maintStation4;
            addOil.textContent = item.maintStation5 + ' л/мес';
            oilPrice.textContent = item.maintStation6 + ' руб.';
            oilFullPrice.textContent = item.maintStation7 + ' руб.';
            calcMaintenance(+parseFloat(candlePrice.textContent), +parseFloat(filters.textContent), +parseFloat(oilCapacity.textContent), +parseFloat(changeOil.textContent), +parseFloat(oilPrice.textContent), +parseFloat(addOil.textContent));

            P.value = calcP(elementRadio.textContent);
            rate.textContent = calcRate(elementRadio.textContent);
            perMonth.textContent = calcPerMonth(rate.textContent);
            sumMonth.textContent = calcSumMonth(current[0].getElementsByTagName('input')[0].value, parseInt(perMonth.textContent)) + ' руб.';
            spendForOne.textContent = spendingForOne(parseInt(sumMonth.textContent), parseInt(maintenancePrice.textContent), parseInt(P.value)) + ' руб.';
            spendEachMonth.textContent = currentSpending(parseInt(P.value), parseInt(priceForP.value)) + ' руб.';
            spendWithGpu.textContent = spendWithGPU(parseInt(sumMonth.textContent), parseInt(maintenancePrice.textContent)) + ' руб.';
            lux.textContent = economy(parseInt(spendEachMonth.textContent), parseInt(spendWithGpu.textContent)) + ' руб.';
            paybackTime.textContent = payback(item.price, lux.textContent) + ' месяцев';
            // -------------INPUT-------------
            current.forEach(element => {
              let currentInput = element.getElementsByTagName('input')[0];
              currentInput.addEventListener('change', () => {
                // P.value = calcP(elementRadio.textContent);
                // rate.textContent = calcRate(elementRadio.textContent);
                let getRadio = 0;
                allRadioBtn.forEach(element => {
                  let input = element.getElementsByTagName('input')[0];
                  if (input.checked) {
                    getRadio = element.textContent;
                  }
                })
                P.value = calcP(getRadio);
                perMonth.textContent = calcPerMonth(parseInt(rate.textContent));
                sumMonth.textContent = calcSumMonth(parseInt(current[0].getElementsByTagName('input')[0].value), parseInt(perMonth.textContent)) + ' руб.';
                spendForOne.textContent = spendingForOne(parseInt(sumMonth.textContent), parseInt(maintenancePrice.textContent), +P.value) + ' руб.';
                spendEachMonth.textContent = currentSpending(+P.value, +priceForP.value) + ' руб.';
                spendWithGpu.textContent = spendWithGPU(parseInt(sumMonth.textContent), parseInt(maintenancePrice.textContent)) + ' руб.';
                lux.textContent = economy(parseInt(spendEachMonth.textContent), parseInt(spendWithGpu.textContent)) + ' руб.';
                // !!!!!!!!!!!!!!!!!!!

                paybackTime.textContent = payback(item.price, parseInt(lux.textContent)) + ' месяцев';
              });
            })
            // -------------------------------
          }
        })
      })
    })


    // P.value = calcP(elementRadio.textContent);
    // rate.textContent = calcRate(elementRadio.textContent);
    // perMonth.textContent = calcPerMonth(parseInt(rate.textContent));
    // sumMonth.textContent = calcSumMonth(current[0].getElementsByTagName('input')[0].value, parseInt(perMonth.textContent)) + ' руб.';
    // spendForOne.textContent = spendingForOne(parseInt(sumMonth.textContent), maintenancePrice.textContent, P.value) + ' руб.';
    // spendEachMonth.textContent = currentSpending(P.value, priceForP.value) + ' руб.';
    // spendWithGpu.textContent = spendWithGPU(parseInt(sumMonth.textContent), maintenancePrice.textContent) + ' руб.';
    // lux.textContent = economy(spendEachMonth.textContent, spendWithGpu.textContent) + ' руб.';
    // paybackTime.textContent = payback(3100000, lux.textContent) + ' месяцев';

  })
});
//-----------------------------



// // -------------INPUT-------------
// current.forEach(element => {
//   let currentInput = element.getElementsByTagName('input')[0];
//   currentInput.addEventListener('change', () => {
//     // P.value = calcP(elementRadio.textContent);
//     // rate.textContent = calcRate(elementRadio.textContent);
//     let getRadio = 0;
//     allRadioBtn.forEach(element => {
//       let input = element.getElementsByTagName('input')[0];
//       if (input.checked) {
//         getRadio = element.textContent;
//       }
//     })
//     P.value = calcP(getRadio);
//     perMonth.textContent = calcPerMonth(parseInt(rate.textContent));
//     sumMonth.textContent = calcSumMonth(parseInt(current[0].getElementsByTagName('input')[0].value), parseInt(perMonth.textContent)) + ' руб.';
//     spendForOne.textContent = spendingForOne(parseInt(sumMonth.textContent), parseInt(maintenancePrice.textContent), +P.value) + ' руб.';
//     spendEachMonth.textContent = currentSpending(+P.value, +priceForP.value) + ' руб.';
//     spendWithGpu.textContent = spendWithGPU(parseInt(sumMonth.textContent), parseInt(maintenancePrice.textContent)) + ' руб.';
//     lux.textContent = economy(parseInt(spendEachMonth.textContent), parseInt(spendWithGpu.textContent)) + ' руб.';
//     // !!!!!!!!!!!!!!!!!!!

//     paybackTime.textContent = payback(3100000, parseInt(lux.textContent)) + ' месяцев';
//   });
// })
// // -------------------------------

// --------------CHECKBOX-----------------

// --------------CHECKBOX-----------------


// D
let calcP = (A) => A * 24 * 30;
// E
let calcRate = (A) => 26 * A / 100;
// F
let calcPerMonth = (E) => E * 24 * 30;
// G
let calcSumMonth = (F, B) => F * B;

// H
let calcMaintenance = (a, b, c, d, e, f) => {
  maintenancePrice.textContent = +a + +b + (c * d * e + f * e) + ' руб.';
}
let spendingForOne = (G, H, D) => ((+G + +H) / +D).toFixed(2);
// K
let currentSpending = (D, C) => D * C;
// L
let spendWithGPU = (G, H) => +G + +H;
// X
let economy = (K, L) => +parseInt(K) - +parseInt(L);
// Y
let payback = (M, X) => (+parseInt(M) / +parseInt(X)).toFixed(1);






// -------STATIONS 100-------
let DOOSAN100 = {
  name: 'DOOSAN 100 кВт',
  price: 3100000,
  opt1: 380000,
  opt2: 1600000,
  opt3: 650000,
  opt4: 125000,
  opt5: 150000,
  opt6: 380000,
  maintStation1: 6000,
  maintStation2: 4500,
  maintStation3: 15.5,
  maintStation4: 1,
  maintStation5: 16.94,
  maintStation6: 100,
  maintStation7: 3244
}
let stations100 = [DOOSAN100]
// -------STATIONS 150-------
let DOOSAN150 = {
  name: 'DOOSAN 150 кВт',
  price: 4370000,
  opt1: 415000,
  opt2: 1700000,
  opt3: 700000,
  opt4: 125000,
  opt5: 150000,
  opt6: 380000,
  maintStation1: 6000,
  maintStation2: 4000,
  maintStation3: 23,
  maintStation4: 1,
  maintStation5: 25.41,
  maintStation6: 100,
  maintStation7: 4841
}
let KMZ150 = {
  name: 'КАМАЗ 150 кВт',
  price: 2750000,
  opt1: 415000,
  opt2: 1700000,
  opt3: 700000,
  opt4: 125000,
  opt5: 150000,
  opt6: 380000,
  maintStation1: 8000,
  maintStation2: 4000,
  maintStation3: 32,
  maintStation4: 1,
  maintStation5: 25.41,
  maintStation6: 100,
  maintStation7: 5741
}
let YAMZ150 = {
  name: 'ЯМЗ 150 кВт',
  price: 2750000,
  opt1: 415000,
  opt2: 1700000,
  opt3: 700000,
  opt4: 125000,
  opt5: 150000,
  opt6: 380000,
  maintStation1: 8000,
  maintStation2: 2500,
  maintStation3: 29,
  maintStation4: 1,
  maintStation5: 25.41,
  maintStation6: 100,
  maintStation7: 5441
}
let stations150 = [DOOSAN150, KMZ150, YAMZ150]
// -------STATIONS 200-------
let DOOSAN200 = {
  name: 'DOOSAN 200 кВт',
  price: 5200000,
  opt1: 480000,
  opt2: 1800000,
  opt3: 750000,
  opt4: 270000,
  opt5: 150000,
  opt6: 380000,
  maintStation1: 8000,
  maintStation2: 4500,
  maintStation3: 21,
  maintStation4: 1,
  maintStation5: 33.88,
  maintStation6: 100,
  maintStation7: 5488
}
let YAMZ200 = {
  name: 'ЯМЗ 200 кВт',
  price: 3490000,
  opt1: 480000,
  opt2: 1800000,
  opt3: 750000,
  opt4: 270000,
  opt5: 150000,
  opt6: 380000,
  maintStation1: 8000,
  maintStation2: 3000,
  maintStation3: 32,
  maintStation4: 1,
  maintStation5: 33.88,
  maintStation6: 100,
  maintStation7: 6588
}
let stations200 = [DOOSAN200, YAMZ200]
// -------STATIONS 250-------
let DOOSAN250 = {
  name: 'DOOSAN 250 кВт',
  price: 6490000,
  opt1: 500000,
  opt2: 1900000,
  opt3: 800000,
  opt4: 295000,
  opt5: 150000,
  opt6: 380000,
  maintStation1: 10000,
  maintStation2: 6500,
  maintStation3: 35,
  maintStation4: 1,
  maintStation5: 42.35,
  maintStation6: 100,
  maintStation7: 7735
}
let TMZ250 = {
  name: 'ТМЗ 250 кВт',
  price: 4950000,
  opt1: 500000,
  opt2: 1900000,
  opt3: 800000,
  opt4: 295000,
  opt5: 150000,
  opt6: 380000,
  maintStation1: 8000,
  maintStation2: 3500,
  maintStation3: 33,
  maintStation4: 1,
  maintStation5: 42.35,
  maintStation6: 100,
  maintStation7: 7535
}
let stations250 = [DOOSAN250, TMZ250]
// -------STATIONS 300-------
let DOOSAN300 = {
  name: 'DOOSAN 300 кВт',
  price: 7700000,
  opt1: 550000,
  opt2: 2000000,
  opt3: 850000,
  opt4: 320000,
  opt5: 150000,
  opt6: 380000,
  maintStation1: 12000,
  maintStation2: 6500,
  maintStation3: 40,
  maintStation4: 1,
  maintStation5: 50.82,
  maintStation6: 100,
  maintStation7: 9082
}
let YAMZ300 = {
  name: 'ЯМЗ 300 кВт',
  price: 5400000,
  opt1: 550000,
  opt2: 2000000,
  opt3: 850000,
  opt4: 320000,
  opt5: 150000,
  opt6: 380000,
  maintStation1: 12000,
  maintStation2: 3500,
  maintStation3: 39,
  maintStation4: 1,
  maintStation5: 50.82,
  maintStation6: 100,
  maintStation7: 8982
}
let stations300 = [DOOSAN300, YAMZ300]
// -------STATIONS 450-------
let MotBaud450 = {
  name: 'Moteurs Baudouin 450 кВт',
  price: 11250000,
  opt1: 580000,
  opt2: 2500000,
  opt3: 1000000,
  opt4: 160000,
  opt5: 250000,
  opt6: 380000,
  maintStation1: 12000,
  maintStation2: 6500,
  maintStation3: 113,
  maintStation4: 1,
  maintStation5: 76.24,
  maintStation6: 100,
  maintStation7: 18.924
}
let stations450 = [MotBaud450]
// -------STATIONS 500-------
let MotBaud500 = {
  name: 'Moteurs Baudouin 500 кВт',
  price: 12800000,
  opt1: 630000,
  opt2: 2800000,
  opt3: 1100000,
  opt4: 180000,
  opt5: 250000,
  opt6: 380000,
  maintStation1: 12000,
  maintStation2: 6500,
  maintStation3: 113,
  maintStation4: 1,
  maintStation5: 84.71,
  maintStation6: 100,
  maintStation7: 19.771
}
let stations500 = [MotBaud500]
// -------STATIONS 650-------
let MotBaud650 = {
  name: 'Moteurs Baudouin 650 кВт',
  price: 16500000,
  opt1: 650000,
  opt2: 3200000,
  opt3: 1200000,
  opt4: 175000,
  opt5: 250000,
  opt6: 380000,
  maintStation1: 12000,
  maintStation2: 7000,
  maintStation3: 146,
  maintStation4: 1,
  maintStation5: 110.12,
  maintStation6: 100,
  maintStation7: 25.612
}
let stations650 = [MotBaud650]
// -------STATIONS 700-------
let MotBaud700 = {
  name: 'Moteurs Baudouin 700 кВт',
  price: 17600000,
  opt1: 700000,
  opt2: 3350000,
  opt3: 1300000,
  opt4: 180000,
  opt5: 250000,
  opt6: 380000,
  maintStation1: 12000,
  maintStation2: 7000,
  maintStation3: 146,
  maintStation4: 1,
  maintStation5: 118.59,
  maintStation6: 100,
  maintStation7: 26459
}
let stations700 = [MotBaud700]
// -------STATIONS 750-------
let MotBaud750 = {
  name: 'Moteurs Baudouin 750 кВт',
  price: 18900000,
  opt1: 750000,
  opt2: 3500000,
  opt3: 1400000,
  opt4: 190000,
  opt5: 250000,
  opt6: 380000,
  maintStation1: 16000,
  maintStation2: 18000,
  maintStation3: 146,
  maintStation4: 1,
  maintStation5: 127.06,
  maintStation6: 100,
  maintStation7: 27.306
}
let stations750 = [MotBaud750]
// -------STATIONS 1000-------
let MotBaud1000 = {
  name: 'Moteurs Baudouin 1000 кВт',
  price: 24900000,
  opt1: 1000000,
  opt2: 3700000,
  opt3: 1600000,
  opt4: 200000,
  opt5: 350000,
  opt6: 380000,
  maintStation1: 16000,
  maintStation2: 24000,
  maintStation3: 230,
  maintStation4: 1,
  maintStation5: 169.41,
  maintStation6: 100,
  maintStation7: 39.941
}
let stations1000 = [MotBaud1000]

let allStations = [stations100, stations150, stations200, stations250, stations300, stations450, stations500, stations650, stations700, stations750, stations1000];